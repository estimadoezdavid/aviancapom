package com.automation.qa.avianca.test;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.qa.avianca.Util;
import com.automation.qa.avianca.page.FlyDetailsPage;

public class TestFlys {

	// objeto utilitario
	private Util util;

	private String nameCurrentMethod;

	// objeto referente al page para traer m�todos recolectores
	private FlyDetailsPage flyDetails;

	// m�todos que se ejecutaran antes
	@BeforeClass
	public void inicializeAutomation() throws IOException {
		util = new Util();
		util.initializeAutomationWeb();
		flyDetails = new FlyDetailsPage(util);
	}

	// m�todos referentes a los test cases
	@Test
	public void searchFly() throws IOException {
		nameCurrentMethod = Thread.currentThread().getStackTrace()[1].getMethodName();
		util.readFile(nameCurrentMethod);
		util.scroll(500);
		flyDetails.searchFly(util.getParameter(1), util.getParameter(2), util.getParameter(3), util.getParameter(4));
	}

	// m�todos que se despu�s
	@AfterClass
	public void closeAutomation() {
		util.getDriver().quit();
	}

}