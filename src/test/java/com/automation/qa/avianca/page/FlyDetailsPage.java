package com.automation.qa.avianca.page;

import java.io.IOException;

import org.openqa.selenium.By;

import com.automation.qa.avianca.Util;

public class FlyDetailsPage {

	// object util
	private Util util;

	// elements or components from the page
	By txtFrom = By.xpath(
			"//*[@id='tab_reservatuvuelo']//div[3]/div[4]/div[1]//div[2]//div[1]/fieldset//div[1]/div/label/div/input[1]");
	By txtTo = By.xpath(
			"//*[@id='tab_reservatuvuelo']//div[3]/div[4]/div[1]//div[2]//div[1]/fieldset//div[2]/label/div/input[1]");
	By txtDateLeft = By.xpath("(//*[@name='pbFechaIdaISO'])[2]");
	By txtDateReturn = By.xpath("(//*[@name='pbFechaRegreso'])[1]");
	By listResultsTarget;

	// build method for inicialize driver
	public FlyDetailsPage(Util util) {
		this.util = util;
	}

	// methods for do interaction with elements
	public void setTxtFrom(String from) {
		util.getDriver().findElement(txtFrom).sendKeys(from);
		clickListResultsTarget(from);
	}

	public void setTxtTo(String to) {
		util.getDriver().findElement(txtTo).sendKeys(to);
		clickListResultsTarget(to);
	}

	public void setTxtDateLeft(String dateLeft) throws IOException {
		util.changePropertyElement(txtDateLeft, "value", dateLeft);
	}

	public void setTxtDateReturn(String dateReturn) {
		util.getDriver().findElement(txtDateReturn).sendKeys(dateReturn);
	}

	public void clickListResultsTarget(String target) {
		listResultsTarget = By.xpath("(//b[text()='" + target + "']//ancestor::li)[1]");
		util.getDriver().findElement(listResultsTarget).click();
	}

	// metodos recolectores -> realizar flujo
	public void searchFly(String from, String to, String dateLeft, String dateReturn) throws IOException {
		setTxtFrom(from);
		setTxtTo(to);
		setTxtDateLeft(dateLeft);
		setTxtDateReturn(dateReturn);
	}

}