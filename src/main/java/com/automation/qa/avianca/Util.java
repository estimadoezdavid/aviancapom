package com.automation.qa.avianca;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

public class Util {

	private String explorer;
	private String pathDriver;
	private String url;
	private String pathParametersFile;
	private WebDriver driver;
	private int timeOut = 20;
	private String downloadPath = "";
	private List<String> colLinea;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void initializeAutomationWeb() throws IOException {
		getProperties();
		initializeBrowser(explorer);
	}

	private void getProperties() throws IOException {
		Properties prop = new Properties();
		InputStream entrada = new FileInputStream("config.properties");
		prop.load(entrada);
		explorer = prop.getProperty("explorer");
		pathDriver = prop.getProperty("pathDriver");
		url = prop.getProperty("urlPage");
		pathParametersFile = prop.getProperty("pathParametersFile");
	}

	private void initializeBrowser(String explorer) {
		switch (explorer) {
		case "chrome":
			getChrome();
			break;
		case "firefox":
//			getFirefox();
			break;
		default:
			break;
		}
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		driver.get(url);
	}

	private void getChrome() {
		System.setProperty("webdriver.chrome.driver", pathDriver);

		ChromeOptions chromeOptions = new ChromeOptions();
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();

		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.PERFORMANCE, Level.ALL);

		chromePrefs.put("download.default_directory", downloadPath);
		chromeOptions.addArguments(
				"" + "--start-maximized " + "--ignore-certificate-errors " + "--disable-extensions " + "--incognito");

		chromeOptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		chromeOptions.setExperimentalOption("prefs", chromePrefs).setExperimentalOption("excludeSwitches",
				new String[] { "enable-automation" });

		driver = new ChromeDriver(chromeOptions);
	}

	public void readFile(String nameTestCase) throws IOException {
		FileReader file = new FileReader(pathParametersFile);
		BufferedReader bRead = new BufferedReader(file);
		colLinea = new ArrayList<String>();
		String linea;
		while ((linea = bRead.readLine()) != null) {
			if (linea.contains(nameTestCase)) {
				colLinea.add(linea);
			}
		}
		bRead.close();
		file.close();
	}

	public String getParameter(int position) {
		String parameter = null;
		String partsLine[] = colLinea.get(colLinea.size() - 1).split(";");
		parameter = partsLine[position];
		return parameter;
	}

	public void scroll(int quantityPixels) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0," + quantityPixels + ")", "");
	}

	public void changePropertyElement(By elementWeb, String property, String value) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(elementWeb);

		js.executeScript("arguments[0].removeAttribute('" + property + "')", element);
		js.executeScript("arguments[0].setAttribute('" + property + "', '" + value + "')", element);
	}

}